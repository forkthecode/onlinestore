﻿using onlineStore.Models;
using System.Collections.Immutable;

namespace onlineStore.Services
{
    public class DeveloperProviderService
    {

        public static readonly ImmutableList<Developer> Developers = CreateRandomData();

        private static ImmutableList<Developer> CreateRandomData()
        {
            int maxRecords = 30;

            List<Developer> developers = new List<Developer>();

            for(int i = 0; i < maxRecords; i++)
            {
                developers.Add(new()
                {
                    Name = Faker.Name.FullName(),
                    SecurityNumber = Faker.Identification.SocialSecurityNumber(),
                    Office = Faker.Country.Name(),
                    PhoneNumber = Faker.Phone.Number(),
                    BirthDate = Faker.Identification.DateOfBirth().ToString(),
                    Currency = Faker.Currency.ThreeLetterCode()

                }) ;
            }

            return ImmutableList.CreateRange(developers);
        }
    }
}
