﻿namespace onlineStore.Models
{
    public class Developer
    {

        public string Name { get; set; } = "";
        public string SecurityNumber { get; set; } = "";
        public string Office { get; set; } = "";
        public String PhoneNumber { get; set; } = "";
        public string BirthDate { get; set; } = "";
        public String Currency { get; set; } = "";
    }
}
